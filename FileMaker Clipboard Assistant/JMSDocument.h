//
//  JMSDocument.h
//  FileMaker Clipboard Assistant
//
//  Created by Jared Sorge on 11/20/13.
//  Copyright (c) 2013 jsorge. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface JMSDocument : NSPersistentDocument
- (IBAction)runSubstitutionButton:(id)sender;
@end
