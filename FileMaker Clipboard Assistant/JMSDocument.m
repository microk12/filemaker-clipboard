//
//  JMSDocument.m
//  FileMaker Clipboard Assistant
//
//  Created by Jared Sorge on 11/20/13.
//  Copyright (c) 2013 jsorge. All rights reserved.
//

#import "JMSDocument.h"
#import "ReplaceRule.h"

@interface JMSDocument ()
@property (strong, nonatomic)NSString *pasteboardType;
@property (unsafe_unretained) IBOutlet NSTextView *textView;
@property (strong) IBOutlet NSArrayController *replaceRulesArrayController;
@end

@implementation JMSDocument

- (id)init
{
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
    }
    return self;
}

- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"JMSDocument";
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
}

+ (BOOL)autosavesInPlace
{
    return YES;
}

#pragma mark - IBActions
/**
 *  Imports the contents of the clipboard and sets the self.pasteboardType to the type set to the clipboard
 *
 *  @param sender not used
 */
- (IBAction)getClipboardButton:(id)sender
{
    NSPasteboard *pb = [NSPasteboard generalPasteboard];
    
    NSArray *pastebordItems = [pb types];
    self.pasteboardType = [pb availableTypeFromArray:pastebordItems];
    NSString *pasteboardString = [pb stringForType:self.pasteboardType];
    
    NSError *error;
    NSXMLDocument *xmlFormat = [[NSXMLDocument alloc] initWithData:[pasteboardString dataUsingEncoding:NSUTF8StringEncoding] options:NSXMLDocumentTidyXML error:&error];
    
    self.textView.string = [xmlFormat XMLStringWithOptions:NSXMLNodePrettyPrint];
}

/**
 *  Performs all substitutions as defined in self.replaceRulesArrayController
 *
 *  @param sender not used
 */
- (IBAction)runSubstitutionButton:(id)sender
{
    NSString *outputString = self.textView.string;
    
    for (ReplaceRule *rule in self.replaceRulesArrayController.arrangedObjects) {
        outputString = [outputString stringByReplacingOccurrencesOfString:rule.searchString withString:rule.replaceString];
    }
    
    self.textView.string = outputString;
}

/**
 *  Copies the text to the clipboard using the self.pasteboardType as the type
 *
 *  @param sender not used
 */
- (IBAction)copyToClipboardButton:(id)sender
{
    NSPasteboard *pb = [NSPasteboard generalPasteboard];
    [pb declareTypes:@[self.pasteboardType] owner:nil];
    
    [pb setString:self.textView.string forType:self.pasteboardType];
}

/**
 *  Performs getClipboardButton:, runSubstitutionButton: and copyToClipboardButton: in one easy step
 *
 *  @param sender not used
 */
- (IBAction)doEverythingButton:(id)sender
{
    [self getClipboardButton:sender];
    [self runSubstitutionButton:sender];
    [self copyToClipboardButton:sender];
}

@end
