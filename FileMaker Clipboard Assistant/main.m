//
//  main.m
//  FileMaker Clipboard Assistant
//
//  Created by Jared Sorge on 11/20/13.
//  Copyright (c) 2013 jsorge. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
