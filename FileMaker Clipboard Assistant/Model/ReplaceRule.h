//
//  ReplaceRule.h
//  FileMaker Clipboard Assistant
//
//  Created by Jared Sorge on 11/20/13.
//  Copyright (c) 2013 jsorge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ReplaceRule : NSManagedObject

@property (nonatomic, retain) NSString * searchString;
@property (nonatomic, retain) NSString * replaceString;
@property (nonatomic, retain) NSNumber * canReplace;

@end
