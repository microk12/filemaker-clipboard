//
//  ReplaceRule.m
//  FileMaker Clipboard Assistant
//
//  Created by Jared Sorge on 11/20/13.
//  Copyright (c) 2013 jsorge. All rights reserved.
//

#import "ReplaceRule.h"


@implementation ReplaceRule

@dynamic searchString;
@dynamic replaceString;
@dynamic canReplace;

@end
